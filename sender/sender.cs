
public static byte ymodem_phase = 1;
public static int cur_file_number = 0;
public static bool phase_two_confirmed = false;
public static string fileName;
public static bool isSendFirstPacket = false;
public static bool didyouResendDataPacket = false;
public static int resendFileNum = 0;
public static List<List<byte>> file_data = new List<List<byte>>();
public static int numberOfSplited;


public static void DataReceived(ref byte[] bytes)
{
    if(UpdatePage.waitC)
    {
        if (bytes[0] == 'C')
        {
            UpdatePage.isStartUpdate = true;
            UpdatePage.waitC = false;
        }
    }

    if (UpdatePage.isStartUpdate)
    {
        ymodem(ref bytes);
    }
}

public static void ymodem(ref byte[] bytes)
{
    if (ymodem_phase == 1)
    {
        // Phase one, the receiver waits for data reception
        if (bytes[0] == 'C' && !UpdatePage.isSendFirstACK)
        {
            List<byte> packet = new List<byte>(1) { Constants.ACK };
            gsp.Write(packet.ToArray());
            UpdatePage.isSendFirstACK = true;
        }
        else if (bytes[0] == 'C' && UpdatePage.isSendFirstACK)
        {
            ymodem_phase++;
            UpdatePage.isSendFirstACK = false;
            // 첫번째 패킷 전송
            SendFirstPacket();
        }
    }
    else if (ymodem_phase == 2)
    {
        // Phase two, the first data packet is sent and confirmed
        if (bytes[0] == Constants.ACK)
        {
            phase_two_confirmed = true;
            SendDataPacket(cur_file_number++);
            ymodem_phase++;
                    
        }
        else if(bytes[0] == Constants.NAK)
        {
            SendFirstPacket();
        }
    }
    else if (ymodem_phase == 3)
    {
        phase_two_confirmed = false;

        if (bytes[0] == Constants.ACK)
        {
            // Phase three, transmission and confirmation of file content
            if(file_data.Count() == cur_file_number)
            {
                // 파일 다 전송한 후 EOT 전송
                byte[] packet = new byte[1] { Constants.EOT };
                gsp.Write(packet);

                // EOT 전송 후 ymdem_phase + 1
                ymodem_phase++;
                return;
            }
            if(didyouResendDataPacket)
            {
                didyouResendDataPacket = false;
            }
            SendDataPacket(cur_file_number++);
            
            
        }
        else if (bytes[0] == Constants.NAK)
        {
            // 방금 전송한 파일 다시 전송
            if(!didyouResendDataPacket)
            {
                resendFileNum = cur_file_number - 1;
            }

            SendDataPacket(resendFileNum);
            didyouResendDataPacket = true;
        }
    }
    else if (ymodem_phase == 4)
    {
        if (bytes[0] == Constants.ACK)
        {
            // 파일 전송 끝
            MessageBox.Show("파일전송 완료");
            ymodem_phase = 1;
            UpdatePage.isStartUpdate = false;
            // 리시버는 다시 C 전송 시작 
        }
        else if (bytes[0] == Constants.NAK)
        {
            // EOT 다시 전송
            byte[] packet = new byte[1] { Constants.EOT };
            gsp.Write(packet);
        }
    }
}

public static void SendFirstPacket()
{
    
    byte[] fileNameByteArr = Encoding.UTF8.GetBytes(fileName);
    byte byteLenOfFileName = (byte)fileNameByteArr.Length;

    List<byte> packet = new List<byte>(1) { Constants.SOH, 0, 0xFF };
    packet.AddRange(fileNameByteArr);

    List<byte> data = Enumerable.Repeat<byte>(0, 128 - byteLenOfFileName).ToList();

    packet.AddRange(data);

    ushort crc_value = Comm.crc16_ccitt(ref data, data.Count());

    packet.Add((byte)(crc_value >> 8));
    packet.Add((byte)(crc_value & 0xFF));

    gsp.Write(packet.ToArray());
}

public static void SendDataPacket(int file_number)
{
    List<byte> packet = new List<byte>(1) { Constants.STX, (byte)(1 + file_number), (byte)(0xFF - (file_number + 1)) };
    List<byte> data = file_data[file_number];

    packet.AddRange(data);

    ushort crc_value = Comm.crc16_ccitt(ref data, data.Count());

    packet.Add((byte)(crc_value >> 8));
    packet.Add((byte)(crc_value & 0xFF));

    gsp.Write(packet.ToArray());
}

