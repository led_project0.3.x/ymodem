#include <vector>
#include "./buffer_serial.h"

using namespace std;

vector<char> file_name;
vector<uint8_t> data_ymodem;
vector<uint8_t> file_data;
int sendedFileNum = 0;
int sendedFileSize = 0;
bool isFirstEOT = true;

uint16_t BufferSerial::get_serial_rx_len(uint16_t head, uint16_t tail)
{
       uint16_t serRxLen = 0;

       if (head != tail) 
       {
              if (head <= tail)          
                     serRxLen = tail - head;
              else
                     serRxLen = tail + MAX_SERIAL_BUF - head;
       }
       return serRxLen;
}


void BufferSerial::y_modem_process()
{ 
       uint8_t* sendData = (uint8_t*)malloc(1);

       uint8_t recv_step  = 0;
       uint8_t seq = 0;
       uint8_t seq_oc = 0xFF;
       int packet_type = 0;
       uint16_t crc = 0;

       uint16_t serRxLen = 0;

       if(recv_callback() == true)
       {
              data_ymodem.clear();

              uint16_t serRxLen = 0;
              serRxLen = get_serial_rx_len(head, tail);
              
              //log_d("ymodem phase : %d", ymodem_phase);

              if(ymodem_phase == 1)
              {
                     byte ack = 0;

                     if(serRxLen != 1)
                     {
                            ack = buf[serRxLen-1];
                     }
                     else
                     {
                            ack = buf[0];
                     }

                     if(ack == ACK)
                     {
                            log_d("ack received");
                            send_c();
                            ymodem_phase = 2;
                     }
              }
              else if(ymodem_phase == 2)
              {
                     data_check();
              }
              else if(ymodem_phase == 3)
              {
                     data_check();
              }
              recv_step = 0;
              head = 0;
              tail = 0;
       } 
}

void BufferSerial::data_check()
{
       if(serRxLen == 1 && buf[0] == EOT)
       {
              // eot
              if(isFirstEOT)
              {
                     Serial.println("EOT");
                     sendData[0] = NAK;
                     send_bt_packet((char*)sendData, 1);
                     isFirstEOT = false;
              }
              else
              {
                     // 파일전송 끝
                     Serial.println("SECOND EOT");
                     sendData[0] = ACK;
                     send_bt_packet((char*)sendData, 1);
                     ymodem_phase = 1;

                     log_d("전송된 파일 개수 : %d", sendedFileNum);
                     log_d("전송된 파일 용량 : %d", sendedFileSize);

                     if(Update.end(true))
                     { 
                            Serial.println("Successful update");  
                     }else 
                     { 
                            Serial.println("Error Occurred: " + String(Update.getError()));
                            return;
                     }
                     
                     Serial.println("Reset in 3 seconds...");
                     delay(3000);
                     
                     ESP.restart();                                   
              }
       }
       else
       {
              for(int i = 0; i < serRxLen; i++)
              {
                     if(recv_step == 0)
                     {
                            if (i == 0 && buf[head + i] == SOH) 
                            { 
                                   // soh
                                   file_name.clear();
                                   log_d("SOH");
                                   recv_step = 1;
                                   packet_type = PACKET_SOH;
                                   continue;
                            }
                            else if(i == 0 && buf[head + i] == STX)
                            {
                                   // stx
                                   //log_d("STX");
                                   recv_step = 1;
                                   packet_type = PACKET_STX;
                                   continue;                                         
                            }                
                     }
                     else if (recv_step == 1) 
                     {
                            if(i == 1)
                            {
                                   // seq
                                   log_d("SEQ");
                                   seq = buf[head+i];
                                   continue;
                            }
                            else if(i == 2)
                            {
                                   // seq_oc
                                   log_d("SEQ-OC");
                                   seq_oc = buf[head+i];
                                   recv_step++;
                                   continue;
                            }                                      
                     }
                     else if(recv_step == 2)
                     {
                            if(packet_type == PACKET_SOH)
                            {
                                   // file name
                                   log_d("file name");
                                   if(buf[head+i] == NULL)
                                   {
                                          recv_step++;

                                          for(int i = 0; i < file_name.size(); i++)
                                          {
                                                 Serial.print((char)file_name[i]);
                                          }
                                          Serial.println("");
                                          data_ymodem.push_back(NULL);
                                          continue;
                                   }
                                   file_name.push_back(buf[head+i]); 
                            }
                     }
                     else if(recv_step == 3)
                     {
                            // soh - data
                            if(packet_type == PACKET_SOH)
                            {
                                   //log_d("soh - data");
                                   data_ymodem.push_back(buf[head+i]);
                                   if(i == serRxLen-1)
                                   {
                                          log_d("soh data + crc h,l 길이 : %d", data_ymodem.size());
                                          log_d("soh - crc");

                                          crc = crc16_ccitt((uint8_t*)&data_ymodem[0], data_ymodem.size());
                                          if (crc == 0) // Crc OK
                                          {
                                                 if(!Update.begin(UPDATE_SIZE_UNKNOWN, U_FLASH, -1, LOW, NULL))
                                                 {
                                                        log_d("Update.begin(UPDATE_SIZE_UNKNOWN, U_FLASH, -1, LOW, NULL) : false (용량 부족)");
                                                 }
                                                 // send ack
                                                 Serial.println("SOH ACK");
                                                 sendData[0] = ACK;
                                                 send_bt_packet((char*)sendData, 1);
                                                 ymodem_phase++;
                                          }
                                          else 
                                          {
                                                 //send NAK
                                                 Serial.println("SOH NAK");
                                                 sendData[0] = NAK;
                                                 send_bt_packet((char*)sendData, 1);
                                                 head = 0;
                                                 tail = 0;
                                                 return;
                                          }
                                   }
                            }
                            else if(packet_type == PACKET_STX)
                            {
                                   //log_d("stx - data");
                                   // stx data
                                   data_ymodem.push_back(buf[head+i]);
                                   if(i == serRxLen-1)
                                   {
                                          if(data_ymodem.size() == serRxLen-3)
                                          {
                                                 crc = crc16_ccitt((uint8_t*)&data_ymodem[0], data_ymodem.size());
                                                 data_ymodem.pop_back();
                                                 data_ymodem.pop_back();
                                                 if (crc == 0) // Crc OK
                                                 {
                                                        //log_d("STX - CRC");
                                                        log_d("STX DATA 길이 : %d", data_ymodem.size());
                                                        // send ack
                                                        //file_data.insert(file_data.end(), data_ymodem.begin(), data_ymodem.end());
                                                        //file_data_vector.push_back(data_ymodem);
                                                        Update.write(&data_ymodem[0], data_ymodem.size());
                                                        Serial.println("STX ACK");
                                                        sendData[0] = ACK;
                                                        send_bt_packet((char*)sendData, 1);
                                                        sendedFileNum++;
                                                        sendedFileSize += data_ymodem.size();
                                                 }
                                                 else
                                                 {
                                                        //send NAK
                                                        Serial.println("STX NAK");
                                                        sendData[0] = NAK;
                                                        send_bt_packet((char*)sendData, 1);
                                                 }
                                          }
                                          else 
                                          {
                                                 //send NAK
                                                 Serial.println("STX NAK");
                                                 sendData[0] = NAK;
                                                 send_bt_packet((char*)sendData, 1);                                                            
                                          }
                                   }
                            }
                     }
              }
       }
}